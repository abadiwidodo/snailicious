﻿using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Snailicious.Api.Client;
using Snailicious.Api.Models.Response;
using Snailicious.Models;

namespace Snailicious.Controllers
{
    public class UserController : Controller
    {
        private UserApiClient userApiClient = new UserApiClient();
        private RecipeApiClient recipeApiClient = new RecipeApiClient();

        public async Task<ActionResult> Index(string userid)
        {
            var user = Mapper.Map<UserResponse, UserProfileModel>(userApiClient.Get(userid));
            var recipes = recipeApiClient.GetByUserId(userid);

            var viewmodel = new UserProfileViewModel();
            viewmodel.UserProfile = user;
            viewmodel.Recipes = recipes;

            return View("Index", viewmodel);
        }

        public async Task<ActionResult> Likes(string userid)
        {
            var user = Mapper.Map<UserResponse, UserProfileModel>(userApiClient.Get(userid));
            var recipes = recipeApiClient.GetLikedByUserId(userid);

            var viewmodel = new UserProfileViewModel();
            viewmodel.UserProfile = user;
            viewmodel.Recipes = recipes;

            return View("Likes", viewmodel);
        }
    }
}