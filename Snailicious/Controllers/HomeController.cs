﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Snailicious.Api.Client;
using Snailicious.Api.Models.Response;
using Snailicious.Models;

namespace Snailicious.Controllers
{
    public class HomeController : Controller
    {
        private readonly RecipeApiClient _recipeApiClient = new RecipeApiClient();

        public ActionResult Index()
        {
            HomeViewModel model = new HomeViewModel();
            model.Recipes = _recipeApiClient.Get(1).ToList();
            return View("Index", model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}