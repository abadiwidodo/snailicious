﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using Snailicious.Api.Client;
using Snailicious.Api.Models.Request;
using Snailicious.Api.Models.Response;
using Snailicious.Models;
using Snailicious.Utilities;

namespace Snailicious.Controllers
{
    public class RecipeController : Controller
    {
        private RecipeApiClient _recipeApiClient = new RecipeApiClient();
        private CommentApiClient _commentApiClient = new CommentApiClient();

        // GET: Recipe
        public ActionResult Index()
        {
            return View();
        }

        // GET: Recipe/Details/5
        public ActionResult Details(int id)
        {
            var model = new RecipeViewModel();
            var recipe = _recipeApiClient.Get(id.ToString());
            model.Recipe = recipe;
            model.IsLiked = recipe.Likes.Exists(l => l.UserId == AccountHelper.CurrentUserId);
            return View("Details", model);  
        }

        // GET: Recipe/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Recipe/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(RecipeRequest recipe)
        {
            try
            {
                _recipeApiClient.Add(recipe);
                return RedirectToAction("Index", "Home");                
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        // GET: Recipe/Edit/5
        public ActionResult Edit(int id)
        {
            var recipe = _recipeApiClient.Get(id.ToString());
            return View("Edit", recipe);
        }

        // POST: Recipe/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, RecipeRequest recipe)
        {
            try
            {
                string result = _recipeApiClient.Update(recipe);
                TempData["UserMessage"] = "Recipe has been updated.";
                return RedirectToAction("Edit", new { id = id });
            }
            catch
            {
                return View();
            }
        }

        // GET: Recipe/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Recipe/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [OutputCache(Duration = 60)]
        public async Task<ActionResult> LoadComments(int recipeId)
        {
            List<CommentResponse> comments = _commentApiClient.GetCommentsByThingId(recipeId.ToString());
            return PartialView("_Comments", comments);
        }

        public async Task<ActionResult> Comment(int id)
        {
            CommentResponse comment = _commentApiClient.GetById(id);  
            return PartialView("_Comment", comment);
        }
    }
}
