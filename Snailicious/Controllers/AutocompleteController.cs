﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.UI;
using Snailicious.Api.Client;

namespace Snailicious.Controllers
{
    [OutputCache(Duration = 3600, Location = OutputCacheLocation.Client, VaryByParam = "none", NoStore = true)]
    public class AutoCompleteController : Controller
    {
        private readonly RecipeApiClient _recipeApiClient = new RecipeApiClient();

        public ActionResult Index(string text)
        {
            var recipes = _recipeApiClient.GetByText(text);      
            var suggestions = new List<TextSuggestion>();
            suggestions.AddRange(recipes.ConvertAll(recipe => new TextSuggestion(recipe.Name, String.Format("/recipe/details/{0}",recipe.Id), "","")));
            return Json(suggestions, JsonRequestBehavior.AllowGet);
        }
    }

    public class TextSuggestion
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Category { get; set; }
        public string Group { get; set; }

        public TextSuggestion(string name, string url, string category, string group)
        {
            Name = name;
            Url = url;
            Category = category;
            Group = group;
        }
    }
}