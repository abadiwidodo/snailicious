﻿using System.Collections.Generic;
using Snailicious.Api.Models.Response;

namespace Snailicious.Models
{
    public class HomeViewModel
    {
        public List<RecipeResponse> Recipes { get; set; }
    }
}