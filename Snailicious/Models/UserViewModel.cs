﻿using System.Collections.Generic;
using Snailicious.Api.Models.Response;

namespace Snailicious.Models
{
    public class UserProfileViewModel
    {
        public UserProfileModel UserProfile { get; set; }
        public List<RecipeResponse> Recipes { get; set; }
    }

    public class UserProfileModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Avatar { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string DisplayName { get; set; }
        public string UserId { get; set; }
    }
}