﻿using Snailicious.Api.Models.Response;

namespace Snailicious.Models
{
    public class RecipeViewModel
    {
        public RecipeResponse Recipe { get; set; }
        public bool IsLiked { get; set; }
    }
}