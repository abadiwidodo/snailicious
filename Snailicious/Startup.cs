﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Snailicious.Startup))]
namespace Snailicious
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
