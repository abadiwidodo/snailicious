﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Snailicious
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "UserProfile",
                url: "user/{userid}",
                defaults: new { controller = "User", action = "Index", userid = "" }
            );

            routes.MapRoute(
                name: "UserLikes",
                url: "user/{userid}/likes",
                defaults: new { controller = "User", action = "Likes", userid = "" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
