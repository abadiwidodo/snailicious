﻿//http://jsfiddle.net/abhiagrawal87/m39xt/
$(".textarealist").focus(function () {
    if ((this).value === '') {
        this.value += '- ';
    }
});

$(".textarealist").keyup(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        this.value += '- ';
    }
    var txtval = this.value;
    if (txtval.substr(txtval.length - 1) == '\n') {
        this.value = txtval.substring(0, txtval.length - 1);
    }
});

var config = {
    plugins: ['remove_button'],
    valueField: 'Url',
    labelField: 'Name',
    searchField: 'Name',
    createOnBlur: true,
    openOnFocus: false,
    options: [],
    items: [],
    delimiter: ';',
    persist: false,
    addPrecedence: true,
    optgroupField: 'Group',
    optgroupLabelField: 'label',
    optgroupValueField: 'value',
    optgroupOrder: ['Region', 'Suburb'],
    create: function (input) {
        return {
            Name: input
        }
    },
    render: {
        option: function (item, escape) {
            return '<div>' +
        '<span class="title">' +
            '<a href="' + escape(item.Url) + '"><span class="name">' + escape(item.Name) + '</span></a>' +
        '</span></div>';
        }
    },
    load: function (query, callback) {
        $.ajax({
            url: '/Autocomplete/',
            type: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            dataType: 'json',
            data: { "text": query, "count": "10" },
            error: function () {
                callback();
            },
            success: function (data) {
                var suggestion = new Array();
                if (data.length == 0) {
                    data[0] = "No results found";
                }
                for (var i = 0; i < data.length ; i++) {
                    suggestion[i] = { label: data[i].Name, Id: data[i].Name };
                }
                callback(data);
            }
        });
    },
    onItemAdd: function (value, item) {
        window.location.href = value;
    },
};

var terms = $("#autocomplete").val();
var termsarray = terms.split(",");

for (var i = 0; i < termsarray.length; i++) {
    config.items[i] = { Name: termsarray[i] };
}

var selectizeinput = $("#autocomplete").selectize(config);
var selectize = selectizeinput[0].selectize;
