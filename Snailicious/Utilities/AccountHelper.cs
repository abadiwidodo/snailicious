﻿using System;
using System.Web;

namespace Snailicious.Utilities
{
    public static class AccountHelper
    {
        public static bool IsAuthenticated
        {
            get
            {
                return HttpContext.Current.User.Identity.IsAuthenticated;
            }
        }

        /// <summary>
        /// The current givenname retrieved from the cookie
        /// </summary>
        public static string CurrentUsername
        {
            get
            {
                HttpCookie myCookie = HttpContext.Current.Request.Cookies["username"];
                if (myCookie != null)
                {
                    string username = myCookie.Value;
                    return username;
                }

                return String.Empty;
            }
        }

        /// <summary>
        /// The current givenname retrieved from the cookie
        /// </summary>
        public static string CurrentUserId
        {
            get
            {
                HttpCookie myCookie = HttpContext.Current.Request.Cookies["userid"];
                if (myCookie != null)
                {
                    string username = myCookie.Value;
                    return username;
                }

                return String.Empty;
            }
        }

        public static string CurrentUserAvatar
        {
            get
            {
                HttpCookie myCookie = HttpContext.Current.Request.Cookies["useravatar"];
                if (myCookie != null)
                {
                    string username = myCookie.Value;
                    return username;
                }

                return String.Empty;
            }
        }

        /// <summary>
        /// The current givenname retrieved from the cookie
        /// </summary>
        public static string CurrentAcessToken
        {
            get
            {
                return HttpContext.Current.User.Identity.Name;
            }
        }

        public static bool AddOrUpdateCurrentUserName(string value)
        {
            return CookieHelper.UpdateCookie("username", value);
        }

        public static bool AddOrUpdateCurrentUserId(string value)
        {
            return CookieHelper.UpdateCookie("userid", value);
        }

        public static bool AddOrUpdateCurrentUserAvatar(string value)
        {
            return CookieHelper.UpdateCookie("useravatar", value);
        }
    }
}