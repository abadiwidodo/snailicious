﻿using System;
using System.Web;

namespace Snailicious.Utilities
{
    public static class CookieHelper
    {
        public static HttpCookie GetCookie(string cookiename)
        {
            HttpCookie myCookie = HttpContext.Current.Request.Cookies[cookiename];
            if (myCookie != null)
            {
                return myCookie;
            }
            return null;
        }

        public static bool AddCookie(string key, string value)
        {
            HttpCookie myCookie = new HttpCookie(key);
            myCookie.Value = value;
            myCookie.Expires = DateTime.Now.AddDays(14d);
            HttpContext.Current.Response.Cookies.Add(myCookie);
            return true;
        }

        public static bool UpdateCookie(string key, string value)
        {
            HttpCookie ck = HttpContext.Current.Request.Cookies[key];
            if (ck == null)
            {
                AddCookie(key, value);
            }
            else if (string.IsNullOrEmpty(ck.Value))
            {
                ck.Value = value;
                HttpContext.Current.Response.Cookies.Set(ck);
                return true;
            }
            return false;
        }

        public static void ClearCookie(string key)
        {
            if (HttpContext.Current.Request.Cookies[key] != null)
            {
                HttpContext.Current.Response.Cookies[key].Expires = DateTime.Now.AddDays(-1);
            }
        }
    }
}